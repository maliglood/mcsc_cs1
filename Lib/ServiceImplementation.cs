﻿using System.Threading;
using System.Threading.Tasks;

namespace Lib
{
    public class ServiceImplementation : IValueSource, IValueProvider
    {
        const int ENDLESS_CYCLE_DEFENCE_TICKS = 1_000_000;

        private bool _isPreffixSet = false;

        private bool _isSuffixSet = false;

        private string _prefix = "";

        private string _suffix = "";

        public Task<string> GetValueAsync(string prefix)
        {
            _prefix = prefix;
            _isPreffixSet = true;

            return Task.Factory.StartNew(() => {
                for (var i = 0; i < ENDLESS_CYCLE_DEFENCE_TICKS; i++)
                {
                    if (!_isPreffixSet || !_isSuffixSet)
                    {
                        Thread.Sleep(100);
                    } else
                    {
                        _isPreffixSet = false;
                        _isSuffixSet = false;
                        return _prefix + _suffix;
                    }
                }

                throw new System.Exception("Endless defence throwed this exception");
            });
        }

        public void SetValue(string suffix)
        {
            _isSuffixSet = true;
            _suffix = suffix;
        }
    }
}
